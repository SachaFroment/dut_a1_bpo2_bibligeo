package element;
/**
 * Classe Caract�re qui �tend Element
 * D�finie par un Character
 */
public class Caract�re extends ElementCoordonn� {

	private Character c;
	
	/**Initialise un Character � une position donn�e*/
	public Caract�re(Character c, int x, int y) {
		super(x, y);
		this.setC(c);
	}
	/**Initialise un Character avec un autre Caracter*/
	public Caract�re(Caract�re ch) {
		this(new Character(ch.c), ch.getX(), ch.getY());
		this.encadrer(ch.getEncadrement());
	}

	public Character getC() {
		return new Character(c.charValue());
	}

	public void setC(Character c) {
		this.c = new Character(c);
	}
	
	/**
	 * m�thode seDessiner : dessine un caract�re si il est pla�able, et son encadrement si il est encadr�.
	 * 
	 * @param imgTmp
	 *            Un tableau � 2 dimensions ou l'on va dessiner le caract�re           
	 */
	public void seDessiner(char[][] imgTmp) {
		final int len = 2;
		if (this.pla�able(imgTmp, this.getX(), this.getY()))
			imgTmp[this.getY()][this.getX()] = this.c;
		if (this.estEncadr�()) {
			new Cadre(this.getX() - 1, this.getY() + 1, len, len, this.getEncadrement()).seDessiner(imgTmp);
			//new Cadre(this.getX() - 2, this.getY() + 2, len + 2, len + 2, this.getEncadrement()).seDessiner(imgTmp);
		}
	}
	
	/**
	 * @return un clone de l'Element sans sa r�f�rence.
	 *        
	 */
	public ElementCoordonn� clone() {
		return new Caract�re(this);
	}
	
	/**
	 * @return les coordonn�es des points les plus en haut � gauche et en bas � droite de l'Element
	 * 
	 */
	public int[] getCoordExtreme() {
		if (this.estEncadr�())
			return new int[] { this.getX() - 1, this.getY() + 1, this.getX() + 1, this.getY() - 1 };
		else
			return new int[] { this.getX(), this.getY(), this.getX(), this.getY() };
	}
}

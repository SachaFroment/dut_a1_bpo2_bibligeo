package element;
import element.ElementCoordonn�;
import image.Element;
/**
 * Classe Element : implemente une interface Element.
 * D�finit par ses coordonn�s dans l'image
 * Tout Element peut �tre encadr�
 *        
 */
public abstract class ElementCoordonn� implements Element{
	private int x, y;
	private Character encadrement;
	
	/**Initialise un Element avec des coordonn�es x et y donn�es*/
	public ElementCoordonn�(int x, int y) {
		this.x = x;
		this.y = y;
		this.encadrement =  new Character(' ');
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * V�rifie si un Element est encadr�.
	 * Le caract�re qui compose l'encadrement vaut ' ' si l'Element n'est pas encadr�
	 *        
	 */
	public boolean estEncadr�() {
		return this.encadrement != ' ';
	}
	/**
	 * m�thode encadrer : Modifie le caract�re d'encadrement de l'Element 
	 *   
	 */
	public void encadrer(Character c) {
		this.encadrement = new Character(c);
	}
	
	public Character getEncadrement() {
		return new Character(this.encadrement);
	}
	/**
	 * m�thode d�sencadrer : Le caract�re d'encadrement est red�fini comme �tant ' '    
	 */
	public void d�sencadrer() {
		this.encadrement = new Character(' ');
	}
	/**
	 * V�rifie si l'Element est pla�able dans l'image   
	 */
	boolean pla�able(char[][] imgTmp, int x, int y) {
		return y < imgTmp.length && x < imgTmp[0].length && x >= 0 && y >= 0;
	}
	/**
	 * m�thode abstraite qui r�cup�re les coordonn�es des points les plus en haut � gauche et en bas � droite de l'Element
	 * 
	 */
	public abstract int[] getCoordExtreme();
}

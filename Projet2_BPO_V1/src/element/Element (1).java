package element;

import image.Image;

public abstract class Element {

	public Element() {
	
	}
	
	public abstract void seDessiner(char[][] c, int x, int y) throws Exception;


}

package element;

public class Charact�re extends Element {

	private Character c;

	public Charact�re(Character c, int x, int y) {
		super(x, y);
		this.setC(c);
	}
	
	public Charact�re(Charact�re ch) {
		super(ch.getX(), ch.getY());
		this.setC(ch.c);
	}

	public Character getC() {
		return new Character(c.charValue());
	}

	public void setC(Character c) {
		this.c = c;
	}

	public void seDessiner(char[][] tab) {
		tab[this.getX()][this.getY()] = this.c;
	}

	public boolean pla�able(int resX, int resY) {
		return this.getX() < resX && this.getY() < resY;
	}
}

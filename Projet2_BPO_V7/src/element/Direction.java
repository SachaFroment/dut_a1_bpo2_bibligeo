package element;

/**
 * Constante d'�num�ration Direction : Les huit orientations possible d'une ligne
 *        (variation de direction en x, variation de direction en y, coordon�es x et y du point le plus en haut � droite) 
 */
public enum Direction {
	Est(1, 0, 0, 0), NordEst(1, 1, 0, 1), Nord(0, 1, 0, 1), NordOuest(-1, 1, -1, 1), Ouest(-1, 0, -1, 0), SudOuest(-1,
			-1, -1, 0), Sud(0, -1, 0, 0), SudEst(1, -1, 0, 0);

	private final int dx, dy, cx, cy;
	
	/**Initialise les valeurs entre parenth�ses de l'�num�ration
	 * 
	 * (variation de direction en x, variation de direction en y, coordon�es x et y du point le plus en haut � droite)
	 * */
	private Direction(int dx, int dy, int cx, int cy) {
		this.dx = dx;
		this.dy = dy;
		this.cx = cx;
		this.cy = cy;
	}

	public int getDx() {
		return dx;
	}

	public int getDy() {
		return dy;
	}

	public int getCx() {
		return cx;
	}

	public int getCy() {
		return cy;
	}
	/**
	 * m�thode suivant : obtient la direction suivante � partir de la direction actuelle dans l'�num�ration.
	 *        
	 */
	public Direction suivant() {
		return Direction.values()[this.ordinal()] == SudEst ? Est : Direction.values()[this.ordinal() + 1];
	}
	/**
	 * m�thode pr�c�dent : obtient la direction pr�c�dente � partir de la direction actuelle dans l'�num�ration.
	 *        
	 */
	public Direction pr�c�dent() {
		return Direction.values()[this.ordinal()] == Est ? SudEst
				: Direction.values()[this.ordinal() - 1];
	}
}